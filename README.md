#ECOM OPS User service
Tools user service to manage user roles, permissions, preferences

## Prerequisite
1. `brew install rethinkdb`
2. Run `rethinkdb --bind all --http-port 9090`
3. `npm install -g recli`
4. sh setup/db/init.sh
5. Assert `r.db("EComConfigs").table("Configs").get("ecom-ops-user")` exists

## Setup
1. `npm install`
2. `npm start`
3. Your service should be running at port 'http://localhost:1980'

## Run tests
`./node_modules/.bin/mocha tests/**/*.js`
