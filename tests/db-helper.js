'use strict'
const r = require('rethinkdb');
const co = require('co');
var connection = null;
var test_db = 'test';
var server = 'localhost';
var port_no = 28015;

module.exports = {
  truncate: function()  {
    return co(function*() {
      try {
        var conn = yield r.connect({host: server, port: port_no });
        yield r.db(test_db).table("Users").delete().run(conn)
        yield r.db(test_db).table("UserRoles").delete().run(conn)
        yield r.db(test_db).table("RolePermissions").delete().run(conn)
        console.log("Successfully reset all tables");
      } catch (error) {
        console.log(error.msg);
        process.exit();
        throw error;
      }
    });
  }
}
