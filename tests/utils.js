'use strict'
const dependencies = {
  logger: {
    log: () => { },
    error: () => { }
  }
}

module.exports = {
  dependencies
}