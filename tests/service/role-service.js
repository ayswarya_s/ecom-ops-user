const expect = require('chai').expect,
  RoleService = require('../../lib/service/role-service'),
  Errors = require('../../lib/errors/errors'),
  sinon = require('sinon'),
  dependencies = require('../utils').dependencies,
  uuid = require('uuid'),
  fs = require('fs'),
  co = require('co'),
  config = JSON.parse(fs.readFileSync('config/test-config.json')).config,
  DbHelper = require('../db-helper');

describe('Role Service -> ', () => {
  const me = this;
  before(function(done) {

    co(function*() {
      try {
        me.roleService = new RoleService(dependencies, config);
        yield DbHelper.truncate();
        done();
      } catch (error) {
        console.log(error);
        done();
      }
    });
  });

  describe('addNewPermission ->', () => {
    let newPermission, saveSpy, getRoleSpy;
    beforeEach(() => {
      newPermission = {
        "role": "TEST",
        "permission": {
          "coupon": {
            "allow": ["read"],
            "discount": {
              "allow": ["read"]
            }
          }
        }
      }
      saveSpy = sinon.spy(me.roleService.roleAccessor, 'save');
      getRoleSpy = sinon.spy(me.roleService.roleAccessor, 'getByRole');
    });
    afterEach(() => {
      saveSpy.restore();
      getRoleSpy.restore();
    });
    it('Create a new role with given permissions', function (done) {
      me.roleService.addNewPermission(newPermission).then((result) => {
        sinon.assert.calledOnce(getRoleSpy);
        sinon.assert.calledOnce(saveSpy);
        expect(saveSpy.args[0][0]).to.eql(newPermission);
        expect(result.role).to.eql(newPermission.role)
        expect(result.permission).to.eql(newPermission.permission);
        done();
      }, (err) => {
        done(err);
      });
    });
    it('Throw error for already existing role', function (done) {
      co(function* () {
        yield me.roleService.addNewPermission(newPermission);
        done(new Error('save should throw error'));
      }).catch((err) => {
        sinon.assert.calledOnce(getRoleSpy);
        sinon.assert.notCalled(saveSpy);
        expect(err).to.deep.equal(Errors.RoleAlreadyExists);
        done();
      });
    });
  });
  describe('getPermission ->', () => {
    let role, Permission, getSpy;
    before(() => {
      getSpy = sinon.spy(me.roleService.roleAccessor, 'getByRole');
      Permission = {
        "role": "TEST",
        "permission": {
          "coupon": {
            "allow": ["read"],
            "discount": {
              "allow": ["read"]
            }
          }
        }
      }
      role = "TEST";
      invalidRole = "TEST1"
    });
    after(() => {
      getSpy.restore();
    });

    it('Get permissions for a given role', (done) => {
      me.roleService.getPermission(role).then((result) => {
        sinon.assert.calledOnce(getSpy);
        expect(getSpy.args[0][0]).to.eql(role);
        expect(result).to.eql(Permission);
        done();
      }, (err) => {
        done(err);
      })
    });

    it('Throw error for Invalid role', function (done) {
      co(function* () {
        yield me.roleService.getPermission(invalidRole);
        done(new Error('getByRole should throw error'));
      }).catch((err) => {
        sinon.assert.calledTwice(getSpy);
        expect(err).to.deep.equal(Errors.RoleNotFound);
        done();
      });
    });
  });
  describe('updatePermission ->', () => {
    let updatePermission, saveSpy, invalidRole, role, getRoleSpy;
    beforeEach(() => {
      role = "TEST",
      updateData = {
        "permission": {
          "coupon": {
            "allow": ["write"],
            "discount": {
              "allow": ["write"]
            }
          }
        }
      }
      Permission = {
        "role": "TEST",
        "permission": {
          "coupon": {
            "allow": ["read"],
            "discount": {
              "allow": ["read"]
            }
          }
        }
      }
      updatedPermission = {
        "role": "TEST",
        "permission": {
          "coupon": {
            "allow": ["write"],
            "discount": {
              "allow": ["write"]
            }
          }
        }
      }
      invalidRole = "TEST1";
      getRoleSpy = sinon.spy(me.roleService.roleAccessor, 'getByRole');
      saveSpy = sinon.spy(me.roleService.roleAccessor, 'save');
    });
    afterEach(() => {
      saveSpy.restore();
      getRoleSpy.restore();
    });
    it('Throw error for Invalid role', function (done) {
      co(function* () {
        yield me.roleService.updatePermission(invalidRole,updateData);
        done(new Error('save should throw error'));
      }).catch((err) => {
        sinon.assert.calledOnce(getRoleSpy);
        sinon.assert.notCalled(saveSpy);
        expect(err).to.deep.equal(Errors.RoleNotFound);
        done();
      });
    });
    it('Update permissions for a role ', function (done) {
      me.roleService.updatePermission(role, updateData).then((result) => {
        sinon.assert.calledOnce(getRoleSpy);
        sinon.assert.calledOnce(saveSpy);
        expect(saveSpy.args[0][0].permission).to.eql(updatedPermission.permission);
        expect(saveSpy.args[0][0].role).to.eql(updatedPermission.role);
        expect(result.role).to.eql(updatedPermission.role)
        expect(result.permission).to.eql(updatedPermission.permission);
        done();
      }
        , (err) => {
          done(err);
        });

    });
  });
});
