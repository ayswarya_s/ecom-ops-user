const expect = require('chai').expect,
  AuthorisationService = require('../../lib/service/authorisation-service'),
  RoleService = require('../../lib/service/role-service'),
  sinon = require('sinon'),
  r = require('rethinkdb'),
  dependencies = require('../utils').dependencies,
  fs = require('fs'),
  config = JSON.parse(fs.readFileSync('config/test-config.json')).config,
  uuid = require('uuid'),
  co = require('co'),
  _ = require('co-lodash'),
  Errors = require('../../lib/errors/errors'),
  DbHelper = require('../db-helper');

describe('Authorisation Service -> ', () => {
  const me = this;
  const testadmin = {
    "role": "TESTADMIN",
    "permission": {
      "order": {
        "allow": ["write"],
        "fulfill": { "allow": ["write"] }
      }
    }
  }
  const testuser = {
    "role": "TESTUSER",
    "permission": {
      "order": {
        "allow": ["read"],
        "fulfill": { "allow": ["read"] }
      }
    }
  }
  before(function (done) {
    me.authorisationService = new AuthorisationService(dependencies, config);
    me.roleService = new RoleService(dependencies, config);
    co(function* () {
      yield DbHelper.truncate();
      yield me.roleService.addNewPermission(testadmin);
      yield me.roleService.addNewPermission(testuser);
      done();
    });
  });

  describe('addUser ->', () => {
    let newUser, newUserInvalidRoles, userId, history, saveSpy, getRoleSpy, testadmin, testuser;
    beforeEach((done) => {

      userId = "69521114-d36b-41a5-b575-127c4cdb26af";
      newUser = {
        "roles": ["TESTADMIN"],
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315"
      }
      newUserInvalidRoles = {
        "roles": ["invalid"],
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315"
      }
      history = {
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315",
        "id": 1,
        "nextState": {
          "roles": ["TESTADMIN"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        },
        "previousState": {}
      }

      getRoleSpy = sinon.spy(me.authorisationService.roleAccessor, 'getByRole');
      saveSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'save');
      getUserSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'getByUserId');
      done();
    });
    afterEach(function (done) {
      saveSpy.restore();
      getRoleSpy.restore();
      getUserSpy.restore();
      done();
    });

    it('Create a new user with given roles and userId', function (done) {
      me.authorisationService.addUser(userId, newUser).then((result) => {
        sinon.assert.calledOnce(getRoleSpy);
        sinon.assert.calledOnce(getUserSpy);
        sinon.assert.calledOnce(saveSpy);
        let sentHistory = _.omit(saveSpy.args[0][0].history[0], ['actionTime']);
        expect(saveSpy.args[0][0].roles).to.eql(newUser.roles);
        expect(sentHistory).to.eql(history);
        expect(result.roles).to.eql(newUser.roles);
        expect(result.user_id).to.eql(userId);
        done();
      }, (err) => {
        done(err);
      });
    });

    it('throw error when trying to use existing userId', (done) => {
      co(function* () {
        yield me.authorisationService.addUser(userId, newUser);
        done(new Error('getByUserId should throw error'))
      }).catch((err) => {
        sinon.assert.calledOnce(getRoleSpy);
        sinon.assert.calledOnce(getUserSpy);
        sinon.assert.notCalled(saveSpy);
        expect(err).to.deep.equal(Errors.UserAlreadyExists);
        done();
      })
    });
    it('throw error when trying to use invalid roles', (done) => {
      co(function* () {
        yield me.authorisationService.addUser(userId, newUserInvalidRoles);
        done(new Error('getByRole should throw error'))
      }).catch((err) => {
        sinon.assert.calledOnce(getRoleSpy);
        sinon.assert.notCalled(getUserSpy);
        sinon.assert.notCalled(saveSpy);
        expect(err).to.deep.equal(Errors.RoleNotFound);
        done();
      })
    });

  });
  describe('getUserRole -> ', () => {
    let userId, roles, getSpy;
    beforeEach(function () {
      userId = "69521114-d36b-41a5-b575-127c4cdb26af";
      roles = ["TESTADMIN"]
      getSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'getByUserId');
    });

    afterEach(function () {
      getSpy.restore();
    });

    it('get the user roles for a given userId', (done) => {
      me.authorisationService.getUserRole(userId).then((result) => {
        sinon.assert.calledOnce(getSpy);
        expect(getSpy.args[0][0]).to.eql(userId);
        expect(result.user_id).to.eql(userId);
        expect(result.roles).to.eql(roles);
        done();
      }, (err) => {
        done(err);
      })
    });
    it('throw error for unknown userId', (done) => {
      let userId = uuid.v4();
      co(function* () {
        yield me.authorisationService.getUserRole(userId);
        done(new Error('getByUserId should throw error'))
      }).catch((err) => {
        sinon.assert.calledOnce(getSpy);
        expect(err).to.deep.equal(Errors.UserNotFound);
        done();
      })
    });
  });
  describe('addUserRole ->', () => {
    let newRoles, invalidRoles, history, userId, saveSpy, getRoleSpy, getUserSpy;
    beforeEach(function () {
      getRoleSpy = sinon.spy(me.authorisationService.roleAccessor, 'getByRole');
      saveSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'save');
      getUserSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'getByUserId');
      userId = "69521114-d36b-41a5-b575-127c4cdb26af";
      newRoles = {
        "roles": ["TESTUSER"],
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315"
      }
      updatedRoles = {
        "roles": ["TESTADMIN", "TESTUSER"]
      }
      invalidRoles = {
        "roles": ["invalid"],
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315"
      }
      history = [{
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315",
        "id": 1,
        "nextState": {
          "roles": ["TESTADMIN"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        },
        "previousState": {}
      },
      {
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315",
        "id": 2,
        "nextState": {
          "roles": ["TESTADMIN", "TESTUSER"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        },
        "previousState": {
          "roles": ["TESTADMIN"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        }
      }]
    });

    afterEach(function () {
      saveSpy.restore();
      getRoleSpy.restore();
      getUserSpy.restore();
    });

    it('throw error when trying to access unknown userId', (done) => {
      let userId = uuid.v4();
      co(function* () {
        yield me.authorisationService.addUserRole(userId, newRoles);
        done(new Error('getUserById should throw error'))
      }).catch((err) => {
        sinon.assert.calledOnce(getUserSpy);
        sinon.assert.notCalled(getRoleSpy);
        sinon.assert.notCalled(saveSpy);
        expect(err).to.deep.equal(Errors.UserNotFound);
        done();
      })
    });
    it('throw error when trying to add invalid roles', (done) => {
      co(function* () {
        yield me.authorisationService.addUserRole(userId, invalidRoles);
        done(new Error('getByRole should throw error'))
      }).catch((err) => {
        sinon.assert.calledOnce(getUserSpy);
        sinon.assert.calledOnce(getRoleSpy);
        sinon.assert.notCalled(saveSpy);
        expect(err).to.deep.equal(Errors.RoleNotFound);
        done();
      })
    });

    it('add the roles for a given userId', (done) => {
      co(function* () {
        let result = yield me.authorisationService.addUserRole(userId, newRoles);
        sinon.assert.calledOnce(getRoleSpy);
        sinon.assert.calledOnce(getUserSpy);
        sinon.assert.calledOnce(saveSpy);
        let sentHistory = saveSpy.args[0][0].history;
        let actualHistory = [];
        _.forEach(sentHistory, function (history) {
          actualHistory.push(_.omit(history, ['actionTime']));
        });
        expect(saveSpy.args[0][0].roles).to.eql(updatedRoles.roles);
        expect(actualHistory[0]).to.eql(history[0]);
        expect(actualHistory[1]).to.eql(history[1]);
        expect(result.roles).to.eql(updatedRoles.roles);
        expect(result.user_id).to.eql(userId);
        done();
      }).catch((err) => {
        done(err);
      })
    });

  });
  describe('getUserPermission ->', () => {
    let userId, permission;
    beforeEach(function () {
      userId = "69521114-d36b-41a5-b575-127c4cdb26af";
      permission = {
        "order": {
          "allow": ["write", "read"],
          "fulfill": {
            "allow": ["write", "read"]
          }
        }
      }
      getRoleSpy = sinon.spy(me.authorisationService.roleAccessor, 'getByRole');
      getUserSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'getByUserId');
    });

    afterEach(function () {
      getUserSpy.restore();
      getRoleSpy.restore();
    });

    it('throw error when trying to access unknown userId', (done) => {
      let userId = uuid.v4();
      co(function* () {
        yield me.authorisationService.getUserPermission(userId);
        done(new Error('getUserById should throw error'))
      }).catch((err) => {
        sinon.assert.calledOnce(getUserSpy);
        sinon.assert.notCalled(getRoleSpy);
        expect(err).to.deep.equal(Errors.UserNotFound);
        done();
      })
    });
    //
    // it('get the permissions for given userId', (done) => {
    //   me.authorisationService.getUserPermission(userId).then((result) => {
    //     sinon.assert.calledOnce(getUserSpy);
    //     sinon.assert.calledTwice(getRoleSpy);
    //     expect(getUserSpy.args[0][0]).to.eql(userId);
    //     expect(getRoleSpy.args[0][0]).to.eql("TESTADMIN");
    //     expect(getRoleSpy.args[1][0]).to.eql("TESTUSER");
    //     expect(result).to.eql(permission);
    //     done();
    //   }, (err) => {
    //     done(err);
    //   });
    // });
  });
  describe('deleteUserRole ->', () => {
    let removeRoles, updatedRoles, history, userId, saveSpy, getUserSpy;
    beforeEach(function () {
      saveSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'save');
      getUserSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'getByUserId');
      userId = "69521114-d36b-41a5-b575-127c4cdb26af";
      removeRoles = {
        "roles": ["TESTUSER"],
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315"
      }
      updatedRoles = {
        "roles": ["TESTADMIN"]
      }
      history = [{
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315",
        "id": 1,
        "nextState": {
          "roles": ["TESTADMIN"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        },
        "previousState": {}
      },
      {
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315",
        "id": 2,
        "nextState": {
          "roles": ["TESTADMIN", "TESTUSER"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        },
        "previousState": {
          "roles": ["TESTADMIN"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        }
      },
      {
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315",
        "id": 3,
        "nextState": {
          "roles": ["TESTADMIN"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        },
        "previousState": {
          "roles": ["TESTADMIN", "TESTUSER"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        }
      }]
    });

    afterEach(function () {
      saveSpy.restore();
      getUserSpy.restore();
    });

    it('throw error when trying to access unknown userId', (done) => {
      let userId = uuid.v4();
      co(function* () {
        yield me.authorisationService.deleteUserRole(userId, removeRoles);
        done(new Error('getUserById should throw error'))
      }).catch((err) => {
        sinon.assert.calledOnce(getUserSpy);
        sinon.assert.notCalled(saveSpy);
        expect(err).to.deep.equal(Errors.UserNotFound);
        done();
      })
    });

    // it('delete the roles for a given userId', (done) => {
    //   co(function* () {
    //     let result = yield me.authorisationService.deleteUserRole(userId, removeRoles);
    //     sinon.assert.calledOnce(getUserSpy);
    //     sinon.assert.calledOnce(saveSpy);
    //     let sentHistory = saveSpy.args[0][0].history;
    //     let actualHistory = [];
    //     _.forEach(sentHistory, function (history) {
    //       actualHistory.push(_.omit(history, ['actionTime']));
    //     });
    //     expect(saveSpy.args[0][0].roles).to.eql(updatedRoles.roles);
    //     expect(actualHistory[0]).to.eql(history[0]);
    //     expect(actualHistory[1]).to.eql(history[1]);
    //     expect(actualHistory[2]).to.eql(history[2]);
    //     expect(result.roles).to.eql(updatedRoles.roles);
    //     expect(result.user_id).to.eql(userId);
    //     done();
    //   }).catch((err) => {
    //     done(err);
    //   })
    // });
  });
  describe('deleteUser ->', () => {
    let history, userId, assignedBy, saveSpy, getUserSpy;
    beforeEach(function () {
      saveSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'save');
      getUserSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'getByUserId');
      userId = "69521114-d36b-41a5-b575-127c4cdb26af";
      assignedBy = "874e2871-81f8-4a4a-9b88-c4bdcb8d5315";
      history = [{
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315",
        "id": 1,
        "nextState": {
          "roles": ["TESTADMIN"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        },
        "previousState": {}
      },
      {
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315",
        "id": 2,
        "nextState": {
          "roles": ["TESTADMIN", "TESTUSER"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        },
        "previousState": {
          "roles": ["TESTADMIN"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        }
      },
      {
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315",
        "id": 3,
        "nextState": {
          "roles": ["TESTADMIN"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        },
        "previousState": {
          "roles": ["TESTADMIN", "TESTUSER"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        }
      },
      {
        "assignedBy": "874e2871-81f8-4a4a-9b88-c4bdcb8d5315",
        "id": 4,
        "nextState": {
          "roles": [],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        },
        "previousState": {
          "roles": ["TESTADMIN"],
          "user_id": "69521114-d36b-41a5-b575-127c4cdb26af"
        }
      }]
    });

    afterEach(function () {
      saveSpy.restore();
      getUserSpy.restore();
    });

    it('throw error when trying to delete unknown userId', (done) => {
      let userId = uuid.v4();
      co(function* () {
        yield me.authorisationService.deleteUser(userId, assignedBy);
        done(new Error('getUserById should throw error'))
      }).catch((err) => {
        sinon.assert.calledOnce(getUserSpy);
        sinon.assert.notCalled(saveSpy);
        expect(err).to.deep.equal(Errors.UserNotFound);
        done();
      })
    });
  //
  //   it('delete all the roles for a given userId', (done) => {
  //     co(function* () {
  //       let result = yield me.authorisationService.deleteUser(userId, assignedBy);
  //       sinon.assert.calledOnce(getUserSpy);
  //       sinon.assert.calledOnce(saveSpy);
  //       let sentHistory = saveSpy.args[0][0].history;
  //       let actualHistory = [];
  //       _.forEach(sentHistory, function (history) {
  //         actualHistory.push(_.omit(history, ['actionTime']));
  //       });
  //       expect(saveSpy.args[0][0].roles).to.eql([]);
  //       expect(actualHistory[0]).to.eql(history[0]);
  //       expect(actualHistory[1]).to.eql(history[1]);
  //       expect(actualHistory[2]).to.eql(history[2]);
  //       expect(actualHistory[3]).to.eql(history[3]);
  //       expect(result.roles).to.eql([]);
  //       expect(result.user_id).to.eql(userId);
  //       done();
  //     }).catch((err) => {
  //       done(err);
  //     })
  //   });
  });
  // describe('getUserPermission for user without roles->', () => {
  //   let userId, permission;
  //   beforeEach(function () {
  //     userId = "69521114-d36b-41a5-b575-127c4cdb26af";
  //     permission = {};
  //     getRoleSpy = sinon.spy(me.authorisationService.roleAccessor, 'getByRole');
  //     getUserSpy = sinon.spy(me.authorisationService.authorisationAccessor, 'getByUserId');
  //   });
  //
  //   afterEach(function () {
  //     getUserSpy.restore();
  //     getRoleSpy.restore();
  //   });
  //   it('get the permissions for given userId(who has no permissions)', (done) => {
  //     me.authorisationService.getUserPermission(userId).then((result) => {
  //       sinon.assert.calledOnce(getUserSpy);
  //       sinon.assert.notCalled(getRoleSpy);
  //       expect(getUserSpy.args[0][0]).to.eql(userId);
  //       expect(result).to.eql(permission);
  //       done();
  //     }, (err) => {
  //       done(err);
  //     });
  //   });
  // });

});
