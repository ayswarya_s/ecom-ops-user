statements=(
'r.dbList().contains("EComConfigs")
  .do(function(databaseExists) {
    return r.branch(
      databaseExists,
      "EComConfigs already exists",
      r.dbCreate("EComConfigs")
    );
})'

'r.dbList().contains("EComOpsUser")
  .do(function(databaseExists) {
    return r.branch(
      databaseExists,
      "EComOpsUser already exists",
      r.dbCreate("EComOpsUser")
    );
})'

'r.dbList().contains("test")
  .do(function(databaseExists) {
    return r.branch(
      databaseExists,
      "EComOpsUser already exists",
      r.dbCreate("test")
    );
})'

'r.db("EComConfigs").tableCreate("Configs");'
  'r.db("EComConfigs").table("Configs").insert({
    "config": {
      "rethinkdb": {
        "db": "EComOpsUser",
        "host": "localhost",
        "max": 16,
        "min": 4,
        "port": "28015",
        "protocol": "http"
      }
    },
    "id": "ecom-ops-user"
  });'

  'r.db("EComOpsUser").tableCreate("Users")'
  'r.db("test").tableCreate("Users")'
  'r.db("EComOpsUser").tableCreate("RolePermissions")'
  'r.db("test").tableCreate("RolePermissions")'
  'r.db("EComOpsUser").tableCreate("UserRoles")'
  'r.db("test").tableCreate("UserRoles")'
  'r.db("EComOpsUser").table("UserRoles").indexCreate("user_id")'
  'r.db("test").table("UserRoles").indexCreate("user_id")'
  'r.db("EComOpsUser").table("RolePermissions").indexCreate("role")'
  'r.db("test").table("RolePermissions").indexCreate("role")'
)

for index in "${!statements[@]}"
do
  mycommand="recli -h localhost "
  mycommand="$mycommand '${statements[index]}'"
  echo $mycommand
  eval $mycommand
done
