'use strict'

const joi = require('joi');

const idSchema = joi.object({
  id: joi.string().guid().required().description('The unique Id of the user')
});

const assignedBySchema = joi.object({
  assignedBy: joi.string().guid().required().description('The unique Id of the user')
});

const roleNameSchema = joi.object({
  role: joi.string().uppercase().required().description("Name of the role")
});

const permissionSchema = joi.object({
  permission: joi.object().required().description('The permission JSON for roles')
});

const roleSchema = joi.object({
  roles: joi.array().items(joi.string().uppercase()).required().description('The rolenames for the user'),
  assignedBy : joi.string().guid().required().description('The unique Id of the user')
});

const userRoleSchema = joi.object({
  userId: joi.string().guid().required().description('The unique Id of the user'),
  roles: joi.array().items(joi.string().uppercase()).required().description('The rolenames for the user'),
  history: joi.array().items(joi.object()).description('The history JSON for a user')
});

const rolePermissionSchema = joi.object({
  role: joi.string().uppercase().required().description('Name of the role'),
  permission: joi.object().required().description('The permisssion JSON for roles')
});

const loginSchema = joi.object({
  email: joi.string().email().required("User email id"),
  password: joi.string().required("User password")
})

module.exports = {
  loginSchema,
  rolePermissionSchema,
  userRoleSchema,
  idSchema,
  roleSchema,
  permissionSchema,
  roleNameSchema,
  assignedBySchema
};
