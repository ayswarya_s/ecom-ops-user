'use strict'

const BaseHelper = require('ecom-base-lib').BaseHelper,
  repoInfo = require('../../repo-info'),
  AuthorisationService = require('../service/authorisation-service'),
  RoleService = require('../service/role-service');

class AuthorisationRouteHandler extends BaseHelper {
  constructor(dependencies, config) {
    super(dependencies, config);
    this.config = config;
    this.config.appId = repoInfo.name;
  }

  getUserRole(request, reply) {
    let me = this;
    let service = new AuthorisationService(me.dependencies, me.config, request);
    service.getUserRole(request.params.id).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }
  addUser(request, reply) {
    let me = this;
    let service = new AuthorisationService(me.dependencies, me.config, request);
    service.addUser(request.params.id, request.payload).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }
  addUserRole(request, reply) {
    let me = this;
    let service = new AuthorisationService(me.dependencies, me.config, request);
    service.addUserRole(request.params.id, request.payload).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }
  deleteUserRole(request, reply) {
    let me = this;
    let service = new AuthorisationService(me.dependencies, me.config, request);
    service.deleteUserRole(request.params.id, request.payload).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }
  deleteUser(request, reply) {
    let me = this;
    let service = new AuthorisationService(me.dependencies, me.config, request);
    service.deleteUser(request.params.id, request.payload.assignedBy).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }
  getUserPermission(request, reply) {
    let me = this;
    let service = new AuthorisationService(me.dependencies, me.config, request);
    service.getUserPermission(request.params.id).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }

  getPermission(request, reply) {
    let me = this;
    let service = new RoleService(me.dependencies, me.config, request);
    service.getPermission(request.params.role).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }
  addNewPermission(request, reply) {
    let me = this;
    let service = new RoleService(me.dependencies, me.config, request);
    service.addNewPermission(request.payload).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }
  updatePermission(request, reply) {
    let me = this;
    let service = new RoleService(me.dependencies, me.config, request);
    service.updatePermission(request.params.role, request.payload).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }

}

module.exports = AuthorisationRouteHandler;
