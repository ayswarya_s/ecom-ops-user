'use strict';

const repoInfo = require('../../repo-info'),
  AuthorisationRouteHandler = require('./authorisation-route-handler');

class RegisterPrivateRoutes {
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
    this.repoConfig = {};
    repoInfo.updateRepoAutoConfig(this.repoConfig);
    this.repoConfig = this.repoConfig.config;
    this.authorisationRouteHandler = new AuthorisationRouteHandler(dependencies, this.repoConfig);
    this.config.appId = this.repoConfig.appId = repoInfo.name;
  }

  registerRoutes(server) {
    server.log('No private routes for Authorisation service !');
  }
}

module.exports = RegisterPrivateRoutes;
