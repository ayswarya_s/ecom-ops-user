'use strict';

const repoInfo = require('../../repo-info'),
  AuthorisationRouteHandler = require('./authorisation-route-handler'),
  UserRouteHandler = require('./user-route-handler'),
  Schema = require('../schema/database/authorisation-schema');


class RegisterPublicRoutes {
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
    this.repoConfig = {};
    repoInfo.updateRepoAutoConfig(this.repoConfig);
    this.repoConfig = this.repoConfig.config;
    this.authorisationRouteHandler = new AuthorisationRouteHandler(dependencies, this.repoConfig);
    this.userRouteHandler = new UserRouteHandler(dependencies, this.repoConfig);
    this.config.appId = this.repoConfig.appId = repoInfo.name;
  }

  registerRoutes(server) {
    const me = this;
    server.log('Registering public routes for Authorisation service');

    server.route({
      method: 'GET',
      path: '/v1/ops-authorisation/user/{id}/roles',
      config: {
        handler: (request, reply) => me.authorisationRouteHandler.getUserRole(request, reply),
        description: 'Get the roles for a user',
        tags: ['api', 'authorisation'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          params: Schema.idSchema
        },
        response: {
          schema: Schema.userRoleSchema,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'PUT',
      path: '/v1/ops-authorisation/user/{id}/roles',
      config: {
        handler: (request, reply) => me.authorisationRouteHandler.addUserRole(request, reply),
        description: 'Add new roles for a user',
        tags: ['api', 'authorisation'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          params: Schema.idSchema,
          payload: Schema.roleSchema
        },
        response: {
          schema: Schema.userRoleSchema,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'DELETE',
      path: '/v1/ops-authorisation/user/{id}/roles',
      config: {
        handler: (request, reply) => me.authorisationRouteHandler.deleteUserRole(request, reply),
        description: 'Delete existing roles for a user',
        tags: ['api', 'authorisation'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          params: Schema.idSchema,
          payload: Schema.roleSchema
        },
        response: {
          schema: Schema.userRoleSchema,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'POST',
      path: '/v1/ops-authorisation/user-roles/{id}',
      config: {
        handler: (request, reply) => me.authorisationRouteHandler.addUser(request, reply),
        description: 'Create a user',
        tags: ['api', 'authorisation'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          params : Schema.idSchema,
          payload: Schema.roleSchema
        },
        response: {
          schema: Schema.userRoleSchema,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'DELETE',
      path: '/v1/ops-authorisation/user-roles/{id}',
      config: {
        handler: (request, reply) => me.authorisationRouteHandler.deleteUser(request, reply),
        description: 'Delete a user',
        tags: ['api', 'authorisation'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          params: Schema.idSchema,
          payload: Schema.assignedBySchema
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/v1/ops-authorisation/user/{id}/permissions',
      config: {
        handler: (request, reply) => me.authorisationRouteHandler.getUserPermission(request, reply),
        description: 'Get permission/permissions for a user',
        tags: ['api', 'authorisation'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          params: Schema.idSchema
        },
        response: {
          schema: Schema.rolePermissionSchema,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/v1/ops-authorisation/role/{role}/permissions',
      config: {
        handler: (request, reply) => me.authorisationRouteHandler.getPermission(request, reply),
        description: 'Get permission/permissions for a role',
        tags: ['api', 'authorisation'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          params: Schema.roleNameSchema
        },
        response: {
          schema: Schema.rolePermissionSchema,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'POST',
      path: '/v1/ops-authorisation/role-permissions',
      config: {
        handler: (request, reply) => me.authorisationRouteHandler.addNewPermission(request, reply),
        description: 'Create a new role with permissions',
        tags: ['api', 'authorisation'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          payload: Schema.rolePermissionSchema
        },
        response: {
          schema: Schema.rolePermissionSchema,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'PUT',
      path: '/v1/ops-authorisation/role/{role}/permissions',
      config: {
        handler: (request, reply) => me.authorisationRouteHandler.updatePermission(request, reply),
        description: 'Update permission/permissions for a role',
        tags: ['api', 'authorisation'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          params: Schema.roleNameSchema,
          payload: Schema.permissionSchema
        },
        response: {
          schema: Schema.rolePermissionSchema,
          failAction: 'log'
        }
      }
    });

    // server.route({
    //    method: 'POST',
    //    path: '/v1/users/',
    //    config: {
    //      handler: (request, reply) => me.userRouteHandler.createUser(request, reply),
    //      description: 'Create a user with / without roles',
    //      tags: ['api', 'ops-users'],
    //      state: {
    //        parse: false,
    //        failAction: 'log'
    //      },
    //    }
    //  });
    //
    //  server.route({
    //    method: 'GET',
    //    path: '/v1/users',
    //    config: {
    //      handler: (request, reply) => me.userRouteHandler.getUserPermissions(request, reply),
    //      description: 'Get user permissions',
    //      tags: ['api', 'authorisation'],
    //      state: {
    //        parse: false,
    //        failAction: 'log'
    //      },
    //    }
    //  });
    //
    //  server.route({
    //    method: 'POST',
    //    path: '/v1/user/login',
    //    config: {
    //      handler: (request, reply) => me.userRouteHandler.login(request, reply),
    //      description: 'Login user',
    //      tags: ['api', 'user'],
    //      state: {
    //        parse: false,
    //        failAction: 'log'
    //      }
    //    }
    //  });
  }
}

module.exports = RegisterPublicRoutes;
