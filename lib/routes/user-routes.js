'use strict';

const repoInfo = require('../../repo-info'),
  UserRouteHandler = require('./user-route-handler'),
  Schema = require('../schema/database/authorisation-schema');

class UserRoutes {
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
    this.repoConfig = {};
    repoInfo.updateRepoAutoConfig(this.repoConfig);
    this.repoConfig = this.repoConfig.config;
    this.userRouteHandler = new UserRouteHandler(dependencies, this.repoConfig);
    this.config.appId = this.repoConfig.appId = repoInfo.name;
  }

  registerRoutes(server) {
    const me = this;
    server.log('Registering public routes for Ecom tools user service');

    server.route({
      method: 'POST',
      path: '/v1/user/',
      config: {
        handler: (request, reply) => me.userRouteHandler.createUser(request, reply),
        description: 'Create a user with / without roles',
        tags: ['api', 'ops-users'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/v1/user/permissions',
      config: {
        handler: (request, reply) => me.userRouteHandler.getUserPermissions(request, reply),
        description: 'Get user permissions',
        tags: ['api', 'ops-user'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'POST',
      path: '/v1/user/sso-login',
      config: {
        handler: (request, reply) => me.userRouteHandler.ssoLogin(request, reply),
        description: 'Login user',
        tags: ['api', 'ops-user'],
        state: {
          parse: false,
          failAction: 'log'
        },
        validate: {
          payload: Schema.loginSchema
        }
      }
    });

    server.route({
      method: 'POST',
      path: '/v1/user/sso-authenticate',
      config: {
        handler: (request, reply) => me.userRouteHandler.ssoAuthenticate(request, reply),
        description: 'Authenticate sso token',
        tags: ['api', 'ops-user'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });
  }
}

module.exports = UserRoutes;
