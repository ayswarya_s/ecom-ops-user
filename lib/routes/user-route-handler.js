'use strict'

const BaseHelper = require('ecom-base-lib').BaseHelper,
  repoInfo = require('../../repo-info'),
  UserService = require('../service/user-service');

class UserRouteHandler extends BaseHelper {
  constructor(dependencies, config) {
    super(dependencies, config);
    this.config = config;
    this.config.appId = repoInfo.name;
  }

  createUser(request, reply) {
    let me = this;
    let userService = new UserService(me.dependencies, me.config, request);
    userService.createUser(request.payload).then(function (result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }

  getUserPermissions(request, reply) {
    let me = this;
    let userService = new UserService(me.dependencies, me.config, request);
    userService.getUserPrivileges(request.query).then(function(result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }

  ssoLogin(request, reply) {
    let me = this;
    let userService = new UserService(me.dependencies, me.config, request);
    userService.ssoLogin(request.payload).then(function(result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }

  ssoAuthenticate(request, reply) {
    let me = this;
    let userService = new UserService(me.dependencies, me.config, request);
    userService.ssoAuthenticate(request.payload).then(function(result) {
      me.replySuccess(reply, result);
    }, function (error) {
      me.replyError(reply, error);
    });
  }
}

module.exports = UserRouteHandler;
