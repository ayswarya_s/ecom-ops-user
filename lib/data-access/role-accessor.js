"use strict";

const EntityAccessor = require('ecom-base-lib').EntityAccessor,
  co = require('co'),
  joi = require('joi');

const options = {
  pk: 'role',
  indexFields: [
  ],
  tableName: 'RolePermissions',
  schema: joi.object({
    role: joi.string().required().description('The rolename for the user'),
    permission: joi.object().required().description('The JSON with roles')
  }).unknown(true),
  version_field_name: 'record_version'
};
/**
 * This is the CRUD class for Roles.
 */

class RoleAccessor extends EntityAccessor {
  /*
     * Constructor
     * @param {object} dependencies The object that contains the logger etc.
     * @param {object} config The base config object that has the rethinkdb config.
     */
  constructor(dependencies, config, requestContext) {
    super(options, dependencies, config, false, requestContext);
  }
  _query(queryFilter, indexName) {
    const me = this;
    return co(function* () {
      return yield me.getAllWithIndex(queryFilter, indexName);
    });
  }

  getByRole(role) {
    let me = this;
    return co(function* () {
      try {
        let result = yield me._query(role, 'role');
        if (!result || result.length == 0) {
          return null;
        }
        return result[0];
      } catch (e) {
        me.error('RoleAccessor', 'getByRole', e, role);
        throw e;
      }
    });
  }
  save(role) {
    let me = this;
    let newRole = [];
    return co(function* () {
      try {
        newRole.push(role);
        let result = yield me.saveAll(newRole);
        return result[0];
      } catch (error) {
        me.error('RoleAccessor', 'save', error, role);
        throw error;
      }
    });
  }
}
module.exports = RoleAccessor;