"use strict";

const EntityAccessor = require('ecom-base-lib').EntityAccessor,
  co = require('co'),
  joi = require('joi');

const options = {
  pk: 'user_id',
  indexFields: [
  ],
  tableName: 'UserRoles',
  schema: joi.object({
    user_id: joi.string().guid().required().description('The unique Id of the user'),
    roles: joi.array().items(joi.string()).required().description('The rolenames of the user'),
    history: joi.array().items(joi.object()).description('The history JSON for a user')
  }).unknown(true),
  version_field_name: 'record_version'
};

/**
 * This is the CRUD class for Authorisation.
 */

class AuthorisationAccessor extends EntityAccessor {
  /*
     * Constructor
     * @param {object} dependencies The object that contains the logger etc.
     * @param {object} config The base config object that has the rethinkdb config.
     */

  constructor(dependencies, config, requestContext) {
    super(options, dependencies, config, false, requestContext);
  }

  _query(queryFilter, indexName) {
    const me = this;
    return co(function* () {
      return yield me.getAllWithIndex(queryFilter, indexName);
    });
  }

  getByUserId(userId) {
    let me = this;
    return co(function* () {
      try {
        let result = yield me._query(userId, 'user_id');
        if (!result || result.length === 0) {
          return null;
        }
        return result[0];
      } catch (error) {
        me.error('AuthorisationAccessor', 'getByuserId', error, userId);
        throw error;
      }
    });
  }

}
module.exports = AuthorisationAccessor;
