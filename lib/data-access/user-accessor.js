"use strict";

const EntityAccessor = require('ecom-base-lib').EntityAccessor,
  co = require('co'),
  joi = require('joi');

const options = {
  pk: 'login_id',
  indexFields: [
  ],
  tableName: 'Users',
  schema: joi.object({
    login_id: joi.string().required().description('User login id')
  }).unknown(true),
  version_field_name: 'record_version'
};
/**
 * This is the CRUD class for Roles.
 */

class UserAccessor extends EntityAccessor {
  /*
     * Constructor
     * @param {object} dependencies The object that contains the logger etc.
     * @param {object} config The base config object that has the rethinkdb config.
     */
  constructor(dependencies, config, requestContext) {
    super(options, dependencies, config, false, requestContext);
  }

  getByEmail (email) {
    let me = this;
    return co(function * () {
      try {
        let result = yield me.filter({"login_id": email}, true);
        if (result && result.length > 0) {
          return result[0];
        }
        return null;
      } catch (e) {
        throw e;
      }
    });
  }

}
module.exports = UserAccessor;
