'use strict'

const BaseHelper = require('ecom-base-lib').BaseHelper,
  co = require('co'),
  Errors = require('../errors/errors'),
  Enum = require('../common/enum'),
  UserAccessor = require('../data-access/user-accessor'),
  AuthorisationService = require('./authorisation-service'),
  AuthorisationAccessor = require('../data-access/authorisation-accessor'),
  SSOClient = require('ecom-ops-auth-lib').Clients.SSOClient;

class UserService extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.dependencies = dependencies;
    this.config = config;
    this.userAccessor = new UserAccessor(dependencies, config, requestContext);
    this.authorisationAccessor = new AuthorisationAccessor(dependencies, config, requestContext);
    this.authorisationService = new AuthorisationService(dependencies, config, requestContext);
  }

  createUser(payload) {
    const me = this;
    return co(function* () {
      try {
        me.log('UserService', 'create user', payload);
        let user = yield me.userAccessor.save(payload);
        let obj = {
          user_id: user.id,
          roles: payload.roles || Enum.DEFAULT_USER_ROLES
        }
        return yield me.authorisationAccessor.save(obj);
      } catch (error) {
        me.error('UserService', 'createUser', error, payload);
        throw error;
      }
    });
  }

  getUserPrivileges(params) {
    const me = this;
    return co(function* () {
      try {
        me.log('User Service', 'get permissions of a user', params['email_id']);
        let user = yield me.userAccessor.getByEmail(params['email_id']);

        if (user) {
          return yield me.authorisationService.getUserPermission(user.id);
        } else {
          return yield me.createUser({"login_id": params['email_id']});
        }

      } catch (error) {
        me.error('UserService', 'getUserPermissions', error, params['email_id']);
      }
    });
  }

  ssoLogin(payload) {
    const me = this;
    return co(function* () {
      try {
        me.log('User service', 'login', payload);
        let email = payload.email;
        let ssoClient = new SSOClient(me.dependencies, me.config);
        let result = yield ssoClient.login(payload);
        let permissions = null;
        let user = null;

        if (result["token"]) {
          user = yield me.userAccessor.getByEmail(email);
          if (user) {
            permissions = yield me.authorisationService.getUserPermission(user.id);
          } else {
            user = yield me.createUser({"login_id": email});
          }
          user = {
            id: user.id,
            email: payload.email,
            permissions: permissions
          }
          return user;
        } else {
          throw Errors.UserAuthenticationFailed;
        }
      } catch(error) {
        me.error("User service", 'login', error);
        throw error;
      }
    });
  }

  ssoAuthenticate(token) {
    const me = this;
    return co(function* () {
      try {
        me.log('User service', 'authenticate', token);
        let ssoClient = new SSOClient(me.dependencies, me.config);
        return yield ssoClient.validateUser(token);
      } catch (error) {
        me.error("User Service", 'authenticate', error);
        throw error;
      }
    });

  }

}

module.exports = UserService;
