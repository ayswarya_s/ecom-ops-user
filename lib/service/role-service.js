'use strict'

const BaseHelper = require('ecom-base-lib').BaseHelper,
  co = require('co'),
  _ = require('lodash'),
  Errors = require('../errors/errors'),
  RoleAccessor = require('../data-access/role-accessor');

class RoleService extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.roleAccessor = new RoleAccessor(dependencies, config, requestContext);
  }
  _stripResult(result) {
    return _.pick(result, ['role', 'permission']);
  }
  getPermission(role) {
    const me = this;
    return co(function* () {
      try {
        me.log("RoleService", "getPermission", "get Permissions for role", role);
        let result = yield me.roleAccessor.getByRole(role);
        if (result == null)
          throw Errors.RoleNotFound;
        return me._stripResult(result);
      } catch (error) {
        me.error('RoleService', 'getUserRole', error, role);
        throw error
      }
    });
  }
  addNewPermission(rolePermission) {
    const me = this;
    return co(function* () {
      try {
        me.log("RoleService", "addNewPermission", "creating a new role and their Permissions", rolePermission);
        let existing = yield me.roleAccessor.getByRole(rolePermission.role);
        if (existing) {
          throw Errors.RoleAlreadyExists;
        }
        let result = yield me.roleAccessor.save(rolePermission);
        return me._stripResult(result);
      } catch (error) {
        me.error('RoleService', 'addNewPermission', error, rolePermission);
        throw error;
      }
    });
  }
  updatePermission(role, permission) {
    const me = this;
    return co(function* () {
      try {
        me.log("RoleService", "updatePermission", "updating Permissions for role", role, permission);
        let existing = yield me.roleAccessor.getByRole(role);
        if (!existing) {
          throw Errors.RoleNotFound;
        }
        existing.permission = permission.permission;
        let result = yield me.roleAccessor.save(existing);
        return me._stripResult(result);
      } catch (error) {
        me.error('RoleService', 'updatePermission', error, role, permission);
        throw error;
      }
    });
  }
}
module.exports = RoleService;