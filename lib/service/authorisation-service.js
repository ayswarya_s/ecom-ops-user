'use strict'

const BaseHelper = require('ecom-base-lib').BaseHelper,
  co = require('co'),
  _ = require('co-lodash'),
  Errors = require('../errors/errors'),
  AuthorisationAccessor = require('../data-access/authorisation-accessor'),
  RoleAccessor = require('../data-access/role-accessor');

class AuthorisationService extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.authorisationAccessor = new AuthorisationAccessor(dependencies, config, requestContext);
    this.roleAccessor = new RoleAccessor(dependencies, config, requestContext);
  }
  _stripResult(result) {
    return _.pick(result, ['user_id', 'roles']);
  }
  _createHistory(id, previousState, nextState, assignedBy) {
    let history = { "id": id, "previousState": previousState, "nextState": nextState, "assignedBy": assignedBy, "actionTime": new Date() };
    return history;
  }
  getUserRole(userId) {
    const me = this;
    return co(function* () {
      try {
        me.log("AuthorisationServcie", "getUserRole", "get user Role for given userId", userId);
        let result = yield me.authorisationAccessor.getByUserId(userId);
        if (result == null)
          throw Errors.UserNotFound;
        return me._stripResult(result);
      } catch (error) {
        me.error('AuthorisationService', 'getUserRole', error, userId);
        throw error;
      }
    });
  }
  addUser(userId, newUser) {
    const me = this;
    let user = { user_id: userId, roles: newUser.roles };
    return co(function* () {
      try {
        me.log("AuthorisationService", "addUser", "creating a new user", user);
        yield _.coEach(newUser.roles, function* (role) {
          let temp = yield me.roleAccessor.getByRole(role);
          if (!temp)
            throw Errors.RoleNotFound;
        });

        let existing = yield me.authorisationAccessor.getByUserId(userId);
        if (existing)
          throw Errors.UserAlreadyExists;

        user.history = [];
        user.history.push(me._createHistory(1, {}, me._stripResult(user), newUser.assignedBy));

        let result = yield me.authorisationAccessor.save(user);
        return me._stripResult(result);
      } catch (error) {
        me.error('AuthorisationService', 'addUser', error, user);
        throw error;
      }
    });
  }
  addUserRole(userId, newRoles) {
    const me = this;
    let user = { userId: userId, roles: newRoles.roles };
    return co(function* () {
      try {
        me.log("AuthorisationService", "addUserRole", "updating  user role", user);
        let existing = yield me.authorisationAccessor.getByUserId(userId);
        if (!existing)
          throw Errors.UserNotFound;
        yield _.coEach(newRoles.roles, function* (role) {
          let temp = yield me.roleAccessor.getByRole(role);
          if (!temp)
            throw Errors.RoleNotFound;
        });
        let previousState = _.last(existing.history).nextState;
        existing.roles = _.union(existing.roles, user.roles);
        let nextState = me._stripResult(existing);
        if(!(_.isEqual(previousState, nextState)))
          existing.history.push(me._createHistory((existing.history.length) + 1, previousState, nextState, newRoles.assignedBy));

        let result = yield me.authorisationAccessor.save(existing);
        return me._stripResult(result);
      } catch (error) {
        me.error('AuthorisationService', 'addUserRole', error, user);
        throw error;
      }
    });
  }
  deleteUser(userId, assignedBy) {
    const me = this;
    return co(function* () {
      try {
        me.log("AuthorisationService", "deleteUser", "Remove a user", userId);
        let existing = yield me.authorisationAccessor.getByUserId(userId);
        if (!existing)
          throw Errors.UserNotFound;
        let previousState = _.last(existing.history).nextState;
        existing.roles=[];
        let nextState = me._stripResult(existing);
        if(!(_.isEqual(previousState, nextState)))
          existing.history.push(me._createHistory((existing.history.length) + 1, previousState, nextState, assignedBy));
        let result = yield me.authorisationAccessor.save(existing);
        return me._stripResult(result);
      } catch (error) {
        me.error('AuthorisationService', 'deleteUser', error, userId);
        throw error;
      }
    });
  }
  deleteUserRole(userId, removeRoles) {
    const me = this;
    let user = { userId: userId, roles: removeRoles.roles };
    return co(function* () {
      try {
        me.log("AuthorisationService", "deleteUserRole", "deleting user role", user);
        let existing = yield me.authorisationAccessor.getByUserId(userId);
        if (!existing)
          throw Errors.UserNotFound;

        let previousState = _.last(existing.history).nextState;
        _.forEach(user.roles, function (role) {
          _.remove(existing.roles, function (existingRole) {
            return existingRole === role;
          });
        });
        let nextState = me._stripResult(existing);
        if(!(_.isEqual(previousState, nextState)))
          existing.history.push(me._createHistory((existing.history.length) + 1, previousState, nextState, removeRoles.assignedBy));

        let result = yield me.authorisationAccessor.save(existing);
        return me._stripResult(result);
      } catch (error) {
        me.error('AuthorisationService', 'deleteUserRole', error, user);
        throw error;
      }
    });
  }
  getUserPermission(userId) {
    const me = this;
    return co(function* () {
      let user = yield me.getUserRole(userId);
      let roles = user.roles;

      try {
        me.log("AuthorisationServcie", "getUserPermission", "get user Permissions for given userId", userId);
        let result = {};
        yield _.coEach(roles, function* (role) {
          let permission;
          let temp = yield me.roleAccessor.getByRole(role);
          permission = temp;
          let customizer = (result, permission) => {
            if (_.isArray(result)) {
              result = _.union(result, permission);
              return result;
            }
          }
          _.mergeWith(result, permission, customizer);
        });
        if(!result.permission)
          return result;
        return result.permission;
      } catch (error) {
        me.error('AuthorisationService', 'getUserPermission', error, userId);
      }
    });
  }



}

module.exports = AuthorisationService;
